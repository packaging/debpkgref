# debpkgref - simple Debian packaging reference tool 🍥

*The best way to do packaging is often to look how others did it.*

`debpkgref` is a simple Python 3 CLI module to quickly **download sources of top
N Debian packages** as listed in [Debian Popularity Contest (popcon)][popcon]
for reference and inspection by packagers.

Aside from mass downloading source packages using `apt-get source`, it can
optionally **extract** dir sub-trees from all source packages that have them as
symlinks in a compact layout for even more convenient packaging research.

You can easily `grep`/`ack`/`rg`/🔍 what you need to know about Debian packaging in
resulting directory trees.


## Features 📦

* Parse and **download** most popular Debian packages from [popcon] URL
  (only stream required data) or local file.

  * Works on any `popcon` data such as [Ubuntu popcon].

* Specify `--number` of packages you want and let `debpkgref` download sources for you.

* Optionally `--extract` dir sub-trees from packages (`debian/` and
  `debian/tests/` by default) for more concise file trees and easier file
  search.

  * Extract dir sub-trees as symlinks or recursive copies - control with `-e`/`--extract-style`.

* Proper Python packaging as a module, use any standard tool (such as `pip`) to install.


## Requirements ⚠

**Python 3** is required.

You **need** to have `apt-get` installed on your system in order to download
packaging sources. That is the case for any Debian-based system and it can be
installed on other systems too.

You also **need** to have appropriate `deb-src` entry in
`/etc/apt/sources.list`, for example:

```
deb-src http://deb.debian.org/debian bullseye main
```


## Installation ⚙

`debpkgref` is a standard python package - install it with `pip` or any other
tool of your choice:

```
pip3 install https://gitlab.nic.cz/packaging/debpkgref/-/archive/master/debpkgref-master.tar.gz
```

For development you can use `develop` install to apply changes instantly:

```
git clone https://gitlab.nic.cz/packaging/debpkgref/
cd debpkgref
pip3 install -e .
```

If you think `debpkgref` should be on PyPI or in a Debian package, open an
[Issue][Issues] requesting this, please.


## Command Reference 📜

### See available options

```
debpkgref --help
```

### Download top N packages' sources

Current directory is used as output base by default with sub-directories created
for source packages (`srcpkg`) and any extractions (`debian` and `tests` by default).

You can override base output directory with `-o`/`--out-dir`.

Specify number of packages to download using `-n`/`--number`:

```
debpkgref -n 3
```

Output:

```
├── srcpkgs                <- source packages (apt-get source)
│   ├── acl-2.2.53         <- unpacked source package
│   ├── acl_2.2.53*        <- source package files
│   ├── perl-5.32.1
│   ├── perl_5.32.1*
│   ├── zlib-1.2.11.dfsg
│   └── zlib_1.2.11.dfsg*
├── debian                 <- extracted $PACKAGE/debian dirs
│   ├── acl                <- srcpkgs/acl-2.2.53/debian
│   │   ├── changelog
│   │   ├── control
│   │   ├── rules
│   │   └── ...
│   ├── perl
│   │   ├── changelog
│   │   ├── control
│   │   ├── rules
│   │   └── ...
│   └── zlib
│       ├── changelog
│       ├── control
│       ├── rules
│       └── ...
└── tests                  <- extracted $PACKAGE/debian/tests dirs
    ├── acl                <- srcpkgs/acl-2.2.53/debian/tests
    │   ├── control
    │   ├── test-build
    │   └── ...
    └── perl
        ├── control
        └── ...
```


### Sub-tree extraction

`-x`/`--extract` option can be used multiple times to select a path within a
source package that will symlinked to a separate output directory for easy
inspection of specific packaging files across multiple packages without the
clutter of full sources.

By default, `debian` and `debian/tests` are extracted for convenience,
that's an equivalent of:

```
debpkgref -x debian -x debian/tests
```

Output:

```
├── debian                 <- extracted $PACKAGE/debian dirs
│   ├── acl                <- srcpkgs/acl-2.2.53/debian
│   ├── perl
│   └── ...
└── tests                  <- extracted $PACKAGE/debian/tests dirs
    ├── acl                <- srcpkgs/acl-2.2.53/debian/tests
    ├── perl
    └── ...
```

As you can see, only last part of extracted path is used in output (i.e. only
`tests` from `debian/tests`) and package version/release information was
stripped leaving only package name. This results in file tree that are easier to
navigate and inspect.

You can override default by using the `-x`/`--extract` option:

```
debpkgref -x debian/patches
```

or disable extraction completely by using `-X`/`--no-extract`:

```
debpkgref -X
```

You can also request recursive copy instead of symlinks using `-e copy`.


### Only extract without download (-n 0)

Extraction is performed on all found package sources in `$OUT_DIR/srcpkgs/` regardless of
what was downloaded in this session.

Source package download is skipped when `-n`/`--number` is set to `0`:

```
debpkgref -n 0
```

### Use local popcon file / cache

Use `-p`/`--popcon-file` to select a local [popcon] file:

```
wget https://popcon.debian.org/by_inst
debpkgref -p by_inst
```

### Use custom popcon data

Just pass the URL using `-p`/`--popcon-file`:

```
debpkgref -p https://popcon.ubuntu.com/by_inst
```

## Contribute / Report Bug

Contributions are always welcome including constructive feature requests ❤

Feel free to create a PR on the [repo][debpkgref] or inspect/create [Issues].



[popcon]: https://popcon.debian.org/
[Ubuntu popcon]: https://popcon.ubuntu.com/
[debpkgref]: https://gitlab.nic.cz/packaging/debpkgref
[Issues]: https://gitlab.nic.cz/packaging/debpkgref/-/issues
