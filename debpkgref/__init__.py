#!/usr/bin/python3

import logging
from pathlib import Path
import re
import shutil
import subprocess
import sys

import click
import requests
import validators


# setup logging to stderr
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)
log_handler = logging.StreamHandler(sys.stdout)
log_handler.setLevel(logging.INFO)
log_formatter = logging.Formatter('%(levelname)s: %(message)s')
log_handler.setFormatter(log_formatter)
log.addHandler(log_handler)


# Debian package popularity contest (popcon) by # of installations
POPCON_DATA_URL = 'https://popcon.debian.org/by_inst'

EXTRACT_DEFAULT = ['debian', 'debian/tests']
EXTRACT_STYLES = ['symlink', 'copy']


@click.command()
@click.option('-s', '--start', type=str,
              default='1', show_default=True,
              help="Start with package (rank or name).")
@click.option('-n', '--number', type=int,
              default=5, show_default=True,
              help="Number of packages to fetch.")
@click.option('-p', '--popcon-file', type=str,
              default=POPCON_DATA_URL, show_default=True,
              help="Input popcon file (local or URL).")
@click.option('-o', '--out-dir', default=Path('.'), show_default=True,
              type=click.Path(file_okay=False, dir_okay=True),
              help="Output base directory.")
@click.option('-X', '--no-extract', 'extract', flag_value=[],
              help="Don't extract anything.")
@click.option('-x', '--extract', type=str, multiple=True,
              default=EXTRACT_DEFAULT, show_default=True,
              help="Extract each $SRCPKG/$THIS dir into separate tree.")
@click.option('-e', '--extract-style', type=click.Choice(EXTRACT_STYLES),
              default=EXTRACT_STYLES[0], show_default=True,
              help="Extraction style.")
@click.help_option('-h', '--help',
                   help="Show this message and exit.")
def debpkgref(
        start,
        number,
        popcon_file,
        out_dir,
        extract,
        extract_style):
    out_path = Path(out_dir)
    srcpkgs_path = out_path / 'srcpkgs'

    log.info("Base output path: %s", out_path)

    if number > 0:
        pkgs = parse_popcon_pkgs(popcon_file, start=start, n=number)
        if pkgs:
            log.info("Source packages output path: %s", srcpkgs_path)
            srcpkgs_path.mkdir(parents=True, exist_ok=True)
            log.info("Downloading 📦 using `apt-get source`...")
            for i, pkg in enumerate(pkgs, start=1):
                log.info("📦 %d of %d: %s", i, number, pkg)
                download_srcpkg(pkg, srcpkgs_path)

    if extract:
        extract_subtrees(srcpkgs_path, out_path, trees=extract,
                         style=extract_style)


def parse_popcon_pkgs(popcon_file, start, n):
    start = str(start)
    r = None
    lines = None
    is_url = validators.url(popcon_file)

    if is_url:
        # URL (with lazy loading)
        log.info("Fetching popcon data: %s", popcon_file)
        r = requests.get(popcon_file, stream=True)
        if r.status_code != 200:
            msg = f"popcon download failed with code: {r.status_code} :("
            raise RuntimeError(msg)
        lines = r.iter_lines()
    else:
        # local file
        log.info("Using local popcon data: %s", popcon_file)
        lines = open(popcon_file, 'r')

    log.info("Downloading %d most popular packages starting from %s...",
             n, start)
    pkgs = []
    started = False
    i = 0
    c = 0
    for line in lines:
        if is_url:
            line = line.decode('utf-8')
        m = re.match(r'\d+\s+(\S+)\s+\d+\s+', line)
        if not m:
            continue
        # valid popcon entry
        i += 1
        pkg = m.group(1)

        if not started:
            # --start can match index or pkg name
            if start in [str(i), pkg]:
                started = True
            else:
                continue

        # requested package
        c += 1
        pkgs.append(pkg)
        if c >= n:
            break
    else:
        log.error("Starting package %s not found.", start)

    if r and r.ok:
        r.close()

    return pkgs


def extract_subtrees(
        src_path,
        dst_path,
        trees,
        style=EXTRACT_STYLES[0]):
    log.info("Extracting sub-trees from source packages: %s" % src_path)
    log.info("Extraction style: %s" % style)
    for deb in src_path.glob('*/debian'):
        srcpkg_path = deb.parent
        srcpkg = srcpkg_path.name
        # strip version for clarity and shorter path
        m = re.match(r'(.*)-\d+', srcpkg)
        if m:
            srcpkg = m.group(1)

        for t in trees:
            ex_src_path = srcpkg_path / t
            ex_dst_path = dst_path / ex_src_path.parts[-1] / srcpkg
            if not ex_src_path.exists():
                continue

            log.info("Extracting %s: %s  ->  %s",
                     style, ex_src_path, ex_dst_path)

            if ex_dst_path.is_symlink():
                log.warning("Removing existing symlink: %s", ex_dst_path)
                ex_dst_path.unlink()
            elif ex_dst_path.exists():
                log.warning("Removing existing output dir: %s", ex_dst_path)
                shutil.rmtree(ex_dst_path)
            else:
                ex_dst_path.parent.mkdir(parents=True, exist_ok=True)

            if style == 'copy':
                # recursive copy
                shutil.copytree(ex_src_path, ex_dst_path, symlinks=True)
            else:
                # relative symlink (default)
                ex_src_rel = Path('..') / ex_src_path.absolute().relative_to(
                    dst_path.absolute())
                ex_dst_path.symlink_to(ex_src_rel)


def download_srcpkg(pkg, out_path):
    subprocess.run(['apt-get', 'source', pkg], cwd=out_path)


if __name__ == '__main__':
    debpkgref()
